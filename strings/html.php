<?php
/**
 * replcae quotes to HTML entities by names or numbers
 *
 * @param (string) escaped string value
 * @param (string) default ='number' will be return to number entities you can use ='name' to return name entities
 * Note : don't use ='name' coz (&apos;) (does not work in IE)
 */
function quote2entities($string,$entities_type='number')
{
    $search                     = array("\"","'");
    $replace_by_entities_name   = array("&quot;","&apos;");
    $replace_by_entities_number = array("&#34;","&#39;");
    $do = null;
    if ($entities_type == 'number')
    {
        $do = str_replace($search,$replace_by_entities_number,$string);
    }
    else if ($entities_type == 'name')
    {
        $do = str_replace($search,$replace_by_entities_name,$string);
    }
    else
    {
        $do = addslashes($string);
    }
    return $do;
}

echo quote2entities("I love 'PHP' for ever");
// will return I love 'PHP' for ever in browsere
// but in view code and database will be  I love &#34;PHP&#34; for ever in source
?>